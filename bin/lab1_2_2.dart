import 'package:lab1_2_2/lab1_2_2.dart' as lab1_2_2;
import 'dart:io';

void main()
{
  print(" Enter String : ");
  String? input = stdin.readLineSync();
  String str = input!.toLowerCase();
  final word = str.split(' ');
  for(int i = 0; i < word.length; i++){
    for(int j = 0; j < word.length; j++){
      if(word[i] == word[j]){
        if(i != j)
        word[i] = "";
      }
    }
  }
  print("Removing Duplicate words:");
  for(int i = 0; i < word.length; i++){
    if(word[i] != ""){
      stderr.write(word[i]+" ") ;
      // print(word[i]+"");
    }
  }
}

// {
//  // write( ) function
//  stderr.write( ' This illustrates ' ) ;
//  stdout.write( ' write( ) function. \n ' ) ;
//  // writeln( ) function
//  stdout.writeln( ' This illustrates ' ) ;
//  stdout.writeln( ' writeln( ) function. \n ' ) ;
//  // print( ) function
//  print( ' This illustrates ' ) ;
//  print( ' print( ) function. ' ) ;
// }

